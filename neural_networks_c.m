clear;
clc;
train_data = importdata('train.data', ',');
train_datax = train_data(:,1:127);
train_datay = train_data(:,128:130);
test_data = importdata('test.data', ',');
test_datax = test_data(:,1:127);
test_datay = test_data(:,128:130);

X = linspace(50,500,10);
Ytr = zeros(1,10);
Yte = zeros(1,10);

for xi = 1:10

    weighto = randn(3,X(1,xi)); %3 is no. of output layers
    weighth = randn(X(1,xi),127); %100 is no. of hidden layers

    outputh = zeros(1,X(1,xi));
    outputo = zeros(1,3);
    deltah = zeros(1,X(1,xi));
    deltao = zeros(1,3);

    eta = 0.1;
    epsilon= 0.0005;
    numIter = 0;
    Jtheta = 0;
    Jthetaprev = 0;
    not_converged = true;

    while not_converged

        Jthetaprev = Jtheta;

        for i=1:length(train_data)

            outputh = sigmf(train_datax(i,:)*weighth', [1 0]); 
            outputo = sigmf(outputh*weighto', [1 0]); 

            deltao = outputo.*(1 - outputo).*(outputo-train_datay(i,:));
            deltah = outputh.*(1 - outputh).*(deltao*weighto);

            weighto = weighto - eta*(deltao'*outputh);
            weighth = weighth - eta*(deltah'*train_datax(i,:));

            Jtheta = Jtheta + sum((outputo-train_datay(i,:)).^2);

        end    

        Jtheta = Jtheta/(2*length(train_data));
        not_converged = abs(Jtheta-Jthetaprev)>epsilon;
        numIter = numIter+1;

    end   

    success = 0;

    for i=1:length(test_data)
        outputh = sigmf(test_datax(i,:)*weighth', [1 0]); 
        outputo = sigmf(outputh*weighto', [1 0]);

        if outputo(1)>outputo(2) && outputo(1)>outputo(3) && test_datay(i,1)==1
            success = success+1;
        elseif outputo(3)>outputo(1) && outputo(3)>outputo(2) && test_datay(i,3)==1
            success = success+1;
        elseif outputo(2)>outputo(1) && outputo(2)>outputo(3) && test_datay(i,2)==1
            success = success+1;
        end    

    end

    Yte(1,xi) = success*100/length(test_data);    

    success = 0;

    for i=1:length(train_data)
        outputh = sigmf(train_datax(i,:)*weighth', [1 0]); 
        outputo = sigmf(outputh*weighto', [1 0]);

        if outputo(1)>outputo(2) && outputo(1)>outputo(3) && train_datay(i,1)==1
            success = success+1;
        elseif outputo(3)>outputo(1) && outputo(3)>outputo(2) && train_datay(i,3)==1
            success = success+1;
        elseif outputo(2)>outputo(1) && outputo(2)>outputo(3) && train_datay(i,2)==1
            success = success+1;
        end    

    end

    Ytr(1,xi) = success*100/length(train_data); 
    
    xi
    
end    
    
plot(X,Ytr,X,Yte);
xlabel('No. of Hidden Units');    
ylabel('Accuracy');    
legend('train accuracy','test accuracy');
    
    
    
    
    